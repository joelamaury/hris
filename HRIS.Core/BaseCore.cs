﻿using Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Core
{  

    public class BaseCore
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public static KronosEntities kronosContext = new KronosEntities();
        public static HRISEntities hrisEntities = new HRISEntities();
    }
}
