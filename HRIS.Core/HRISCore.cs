﻿using Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Core
{
    public class HRISCore : BaseCore
    {

        public void Execute()
        {
            logger.Info("Program Started...");
            System.AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            LogManager.ThrowExceptions = true;

            ProcessExtractFiles();

            logger.Info("Program Finished...");
        }

        private static void ProcessExtractFiles()
        {
            ///GetFiles from directory defined in app.config
            logger.Info("Reading Files From: " + ConfigurationManager.AppSettings["ExtractFilesFolder"].ToString());
            string[] files = Directory.GetFiles(ConfigurationManager.AppSettings["ExtractFilesFolder"].ToString());
            logger.Info(files.Count().ToString() + " file(s) read");
            foreach (string path in files)
            {
                logger.Info("--------------------------------------------------------------------------------------------------------------");
                logger.Info("Begining to process file: ");

                ExtractedFile file = new ExtractedFile();
                file.Name = "HRIS-1.0.0-"+Path.GetFileNameWithoutExtension(path) + DateTime.Now.ToFileTimeUtc() + Path.GetExtension(path);
                file.Date = DateTime.Now;

                ///Read all file and get the modules                
                string[] modules = File.ReadAllLines(path);

                ///Exclude empty lines
                List<string> notEmptyModules = new List<string>();
                foreach (string module in modules)
                {
                    string moduleInfo = module.ToString().Trim();
                    if (moduleInfo != string.Empty)
                    {
                        notEmptyModules.Add(moduleInfo);
                    }
                }

                modules = notEmptyModules.ToArray();

                logger.Info("Module count: " + modules.Count().ToString());
                foreach (string module in modules)
                {
                    ///We need to send all the modules because if we detect a NewHire we need to search in all the modules 
                    ///the personal info and employment status for that new hire to process those modules first. This guarantees
                    ///the correct function of this program                   
                    ProcessModule(module, file, modules);
                }

                hrisEntities.ExtractedFiles.Add(file);
                hrisEntities.SaveChanges();

                logger.Info("Finished processing file " + file.Name);
                logger.Info("--------------------------------------------------------------------------------------------------------------");


                File.Move(path, ConfigurationManager.AppSettings["ProcessedFiles"].ToString() + file.Name);
            }

        }

        private static void ProcessModule(string module, ExtractedFile file, string[] modules)
        {
            //Get all fields splitting by separator
            string[] fields = module.Split('|');

            int screenId = 0;
            try
            {
                //Get Module Id
                screenId = int.Parse(fields[0].ToString().Trim());
            }
            catch (Exception ex)
            {
                file.Log += "Invalid module field at position 0, expected ScreenId(int), received: " + fields[0].ToString().Trim() + "#";
                logger.Error(file.Log);
                logger.Error(ex);

                return;
            }

            ///Get Module Info
            Module moduleInfo = hrisEntities.Modules.Where(m => m.Id == screenId).FirstOrDefault();

            ExtractedFileModule extractModule = new ExtractedFileModule();
            extractModule.ModuleId = moduleInfo.Id;
            extractModule.Info = module;

            ProcessModuleFields(fields, moduleInfo, file, extractModule, modules);
        }

        private static void ProcessModuleFields(string[] fields, Module module, ExtractedFile file, ExtractedFileModule extractModule, string[] modules)
        {
            ///Validate correct file count based on Fields defined on Database for specific module
            if (fields.Count() != module.ModuleFields.Count())
            {
                extractModule.Status = "error";
                extractModule.Log += "ModuleCountError: " + module.Name + " expected " + module.ModuleFields.Count().ToString() + " fields, but received " + fields.Count() + "#";
                file.ExtractedFileModules.Add(extractModule);
                logger.Error(extractModule.Log);
            }
            else
            {
                ///Get Main Fields iinfo
                string legacyEmpNum = string.Empty;

                try
                {
                    legacyEmpNum = fields[2].ToString().Trim();
                }
                catch (Exception ex)
                {
                    extractModule.Status = "error";
                    extractModule.Log += "Invalid module field at position 0, expected LegacyEmpNum(int), received: " + fields[2].ToString().Trim() + "#";
                    logger.Error(ex);

                    return;
                }


                ///Search for employee in tEMPLOYMENT_STATUS by EmpNo
                ///If Employee is not found means its a new hire
                int personIdNo = 0;

                tEMPLOYMENT_STATUS empStatus = kronosContext.tEMPLOYMENT_STATUS.Where(e => e.EmpNo == legacyEmpNum.ToString()).FirstOrDefault();

                if (empStatus != null)
                {
                    personIdNo = empStatus.PersonIdNo;
                }
                else
                {
                    ///New Hire detected. Here we need to search in all the modules of the file, first for the PersonalInfo and then for the 
                    ///Employment Status. We need those registers in the database first
                    int result = ProcessNewHire(file, legacyEmpNum, modules);

                    if (result == -1)
                    {
                        extractModule.Log += "No new hire info found on file or error on new hire info. New hire info needs to be processed in order to process " + module.Name + " module info#";
                        extractModule.Status = "error";
                        file.ExtractedFileModules.Add(extractModule);
                        logger.Error("No new hire info found on file or error on new hire info. New hire info needs to be processed in order to process " + module.Name + " module info#");
                        return;
                    }
                }
                ///Check if module already has been processed (normaly this occurs when a module was processed in the new hire method)
                if (CheckModuleProcessed(file, legacyEmpNum, module.Id) == false)
                {
                    logger.Info("Processing module " + module.Name);

                    switch (module.Id)
                    {
                        case 1:
                            {
                                ProcessPersonalInfoModule(fields, module, personIdNo, extractModule, legacyEmpNum);
                                break;
                            }

                        case 2:
                            {
                                ProcessAddressModule(fields, module, personIdNo, extractModule, legacyEmpNum);
                                break;
                            }
                        case 3:
                            {
                                ProcessPhoneModule(fields, module, personIdNo, extractModule, legacyEmpNum);
                                break;
                            }
                        case 4:
                            {
                                ProcessEmailModule(fields, module, personIdNo, extractModule, legacyEmpNum);
                                break;
                            }
                        case 6:
                            {
                                ///PositionId parameter -1 to indicate its not a newHire
                                ProcessEmploymentStatusModule(fields, module, personIdNo, extractModule, legacyEmpNum, -1);
                                break;
                            }
                        case 7:
                            {
                                int salariedHourlyCodeIdNo = 0;
                                ProcessPositionModule(fields, module, personIdNo, extractModule, legacyEmpNum, false, out salariedHourlyCodeIdNo);
                                break;
                            }
                        case 8:
                            {
                                ProcessPayStatusModule(fields, module, personIdNo, extractModule, legacyEmpNum, -1);
                                break;
                            }
                        case 11:
                            {
                               // ProcessParollStatusModule(fields, module, personIdNo, extractModule, legacyEmpNum);
                                break;
                            }
                    }

                    file.ExtractedFileModules.Add(extractModule);

                }

            }
        }

        private static int ProcessNewHire(ExtractedFile file, string legacyEmpNum, string[] modules)
        {
            logger.Info("Processing New Hire");

            string[] personalInfoFields = null;
            string[] employmentStatusFields = null;
            string[] positionFields = null;
            string[] payStatusFields = null;

            string persInfMod = string.Empty;
            string posMod = string.Empty;
            string empMod = string.Empty;
            string payMod = string.Empty;

            foreach (string module in modules)
            {
                string[] fields = module.Split('|');

                int screenId = 0;
                try
                {
                    //Get Module Id
                    screenId = int.Parse(fields[0].ToString().Trim());
                }
                catch (Exception ex)
                {
                    file.Log += "Invalid module field at position 0, expected ScreenId(int), received: " + fields[0].ToString().Trim() + "#";
                    logger.Error(file.Log);
                    logger.Error(ex);

                    return -1;
                }

                string legEmpNum = string.Empty;

                try
                {
                    legEmpNum = fields[2].ToString().Trim();
                }
                catch (Exception ex)
                {
                    file.Log += "Invalid module field at position 0, expected LegacyEmpNum(int), received: " + fields[2].ToString().Trim() + "#";
                    logger.Error(ex);

                    return -1;
                }

                if (screenId == 1 && legacyEmpNum == legEmpNum)
                {
                    ///Found Personal Info
                    personalInfoFields = fields;
                    persInfMod = module;
                }

                if (screenId == 7 && legacyEmpNum == legEmpNum)
                {
                    ///Found Position Info
                    positionFields = fields;
                    posMod = module;
                }

                if (screenId == 6 && legacyEmpNum == legEmpNum)
                {
                    ///Found EmploymentStatus Info
                    employmentStatusFields = fields;
                    empMod = module;
                }

                if (screenId == 8 && legacyEmpNum == legEmpNum)
                {
                    ///Found PayStatus Info
                    payStatusFields = fields;
                    payMod = module;
                }

                if (personalInfoFields != null && positionFields != null && employmentStatusFields != null && payStatusFields != null)
                {
                    break;
                }
            }

            ///If Personal Info module not found log error
            if (personalInfoFields == null)
            {
                logger.Error("New Hire: Personal Info Module not found for LegacyEmpNum " + legacyEmpNum);
                file.Log += "New Hire: Personal Info Module not found for LegacyEmpNum " + legacyEmpNum + "#";
            }

            ///If Position module not found log error
            if (positionFields == null)
            {
                logger.Error("New Hire: Position Module not found for LegacyEmpNum " + legacyEmpNum);
                file.Log += "New Hire: Position Module not found for LegacyEmpNum " + legacyEmpNum + "#";
            }

            ///If EmploymentStatus module not found log error
            if (employmentStatusFields == null)
            {
                logger.Error("New Hire: Employment Status Module not found for LegacyEmpNum " + legacyEmpNum);
                file.Log += "New Hire: Employment Status Module not found for LegacyEmpNum " + legacyEmpNum + "#";
            }

            ///If PayStatus module not found log error
            if (payStatusFields == null)
            {
                logger.Error("New Hire: Pay Status Module not found for LegacyEmpNum " + legacyEmpNum);
                file.Log += "New Hire: Pay Status Module not found for LegacyEmpNum " + legacyEmpNum + "#";
            }

            if (personalInfoFields != null && employmentStatusFields != null && positionFields != null && payStatusFields != null)
            {
                Module personalInfo = hrisEntities.Modules.Where(m => m.Id == 1).FirstOrDefault();
                Module position = hrisEntities.Modules.Where(m => m.Id == 7).FirstOrDefault();
                Module employment = hrisEntities.Modules.Where(m => m.Id == 6).FirstOrDefault();
                Module pay = hrisEntities.Modules.Where(m => m.Id == 8).FirstOrDefault();

                ExtractedFileModule extractPersonal = new ExtractedFileModule();
                extractPersonal.ModuleId = personalInfo.Id;
                extractPersonal.Info = persInfMod;

                ExtractedFileModule extractPosition = new ExtractedFileModule();
                extractPosition.ModuleId = position.Id;
                extractPosition.Info = posMod;

                ExtractedFileModule extractEmployment = new ExtractedFileModule();
                extractEmployment.ModuleId = employment.Id;
                extractEmployment.Info = empMod;

                ExtractedFileModule extractPay = new ExtractedFileModule();
                extractPay.ModuleId = pay.Id;
                extractPay.Info = payMod;

                logger.Info("Processing New Hire module Personal Info");
                ///Validate correct file count based on Fields defined on Database for specific module
                if (personalInfoFields.Count() != personalInfo.ModuleFields.Count())
                {
                    extractPersonal.Status = "error";
                    extractPersonal.Log += "ModuleCountError: " + personalInfo.Name + " expected " + personalInfo.ModuleFields.Count().ToString() + " fields, but received " + personalInfoFields.Count();
                    logger.Error(extractPersonal.Log);
                    return -1;
                }

                if (positionFields.Count() != position.ModuleFields.Count())
                {
                    extractPosition.Status = "error";
                    extractPosition.Log += "ModuleCountError: " + position.Name + " expected " + position.ModuleFields.Count().ToString() + " fields, but received " + positionFields.Count();
                    logger.Error(extractPosition.Log);
                    file.ExtractedFileModules.Add(extractPosition);
                    return -1;
                }

                if (employmentStatusFields.Count() != employment.ModuleFields.Count())
                {
                    extractEmployment.Status = "error";
                    extractEmployment.Log += "ModuleCountError: " + employment.Name + " expected " + employment.ModuleFields.Count().ToString() + " fields, but received " + employmentStatusFields.Count();
                    logger.Error(extractEmployment.Log);
                    file.ExtractedFileModules.Add(extractEmployment);
                    return -1;
                }

                if (payStatusFields.Count() != pay.ModuleFields.Count())
                {
                    extractPay.Status = "error";
                    extractPay.Log += "ModuleCountError: " + pay.Name + " expected " + pay.ModuleFields.Count().ToString() + " fields, but received " + payStatusFields.Count();
                    logger.Error(extractPay.Log);
                    file.ExtractedFileModules.Add(extractPay);
                    return -1;
                }

                int personIdNo = ProcessPersonalInfoModule(personalInfoFields, personalInfo, 0, extractPersonal, legacyEmpNum);
                file.ExtractedFileModules.Add(extractPersonal);
                if (personIdNo == -1)
                {
                    /// Return error creating employee indicator (-1)
                    file.ExtractedFileModules.Add(extractPosition);
                    return -1;
                }


                logger.Info("Processing New Hire module Position");
                /// salariedHourlyCodeIdNo used later to decide wich PayrollStatus create (Exempt or Hourly)
                int salariedHourlyCodeIdNo = 0;
                int positionIdNo = ProcessPositionModule(positionFields, position, personIdNo, extractPosition, legacyEmpNum, true, out salariedHourlyCodeIdNo);

                logger.Info("Processing New Hire module Employment Status");
                ProcessEmploymentStatusModule(employmentStatusFields, employment, personIdNo, extractEmployment, legacyEmpNum, positionIdNo);

                logger.Info("Processing New Hire module Pay Status");
                ProcessPayStatusModule(payStatusFields, pay, personIdNo, extractPay, legacyEmpNum, positionIdNo);

                file.ExtractedFileModules.Add(extractPosition);
                file.ExtractedFileModules.Add(extractEmployment);
                file.ExtractedFileModules.Add(extractPay);

                ///Create DEFAULT Payroll Status Info, send personalInfo fields to get the effectiveDate field
                ProcessDefaultvaluesParollStatusModule(personIdNo, personalInfoFields, salariedHourlyCodeIdNo);

                ///Sync Employee
                ProcessSyncEmployee(personIdNo, positionIdNo, personalInfoFields);

                logger.Info("New Hire required modules processed");

                return 1;
            }
            else
            {
                return -1;
            }
        }

        private static bool CheckModuleProcessed(ExtractedFile file, string legacyEmpNum, int moduleId)
        {
            foreach (ExtractedFileModule child in file.ExtractedFileModules)
            {
                ///Allow program to process multiple Address
                //if (child.ModuleId == 2 || child.ModuleId == 3 || child.ModuleId == 4)
                //{
                //    return false;
                //}

                if (child.ModuleId == moduleId && child.EmpId == legacyEmpNum)
                {
                    return true;
                }
            }

            return false;
        }

        private static void ProcessParollStatusModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum)
        {
            DateTime from;
            string actionIndicator = string.Empty;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }

            try
            {
                from = DateTime.Parse(fields[3].ToString().Trim());
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 2, expected EffectiveFromDate(int), received: " + fields[3].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }
            try
            {
                tPERSON_PAYROLL_SETUPS personPayrollStatus = kronosContext.tPERSON_PAYROLL_SETUPS.Where(p => p.PersonIdNo == personIdNo && p.PersonPayrollSetupStartDate <= from && p.PersonPayrollSetupEndDate >= from).FirstOrDefault();
                if (personPayrollStatus == null)
                {
                    personPayrollStatus = new tPERSON_PAYROLL_SETUPS();
                    personPayrollStatus.PersonIdNo = personIdNo;
                }

                ///Fill Person Payroll Status with values from extracted file
                personPayrollStatus = (tPERSON_PAYROLL_SETUPS)FillObjectValues(fields, module, personPayrollStatus, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos     

                if (personPayrollStatus != null)
                {
                    System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("SeqNo", typeof(Int32));
                    seqNo.Value = personPayrollStatus.PersonPayrollSetupSeqNo;

                    kronosContext.sp_saveEmpPaySetupCtrl(personPayrollStatus.PersonIdNo,
                                                         null,
                                                         seqNo,
                                                         personPayrollStatus.PersonPayrollSetupStartDate,
                                                         personPayrollStatus.PersonPayrollSetupEndDate,
                                                         null,
                                                         personPayrollStatus.PayGroupIdNo,
                                                         personPayrollStatus.ResidenceLocationIdNo,
                                                         personPayrollStatus.BenefitPayGroupInd,
                                                         personPayrollStatus.PayrollStatusCodeIdNo,
                                                         null,
                                                         1,
                                                         null,
                                                         null,
                                                         null,
                                                         null,
                                                         null);

                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK " + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s )" + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
            }
        }

        private static void ProcessSyncEmployee(int personIdNo, int positionIdNo, string[] personalInfoFields)
        {
            try
            {
                DateTime from = DateTime.Today;
                try
                {
                    from = DateTime.Parse(personalInfoFields[3].ToString().Trim());
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

                System.Data.Entity.Core.Objects.ObjectParameter xmlString = new System.Data.Entity.Core.Objects.ObjectParameter("XMLString", typeof(Int32));
                System.Data.Entity.Core.Objects.ObjectParameter supervisorExists = new System.Data.Entity.Core.Objects.ObjectParameter("WFPSupervisorExist", typeof(Int32));

                kronosContext.sp_WTK_SaveSyncEmployee(personIdNo, null, from, from, xmlString, false, positionIdNo, 1, null, false, supervisorExists);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }


        private static void ProcessDefaultvaluesParollStatusModule(int personId,string[] personalInfoModuleFields, int salariedHourlyCodeIdNo)
        {
            try
            {
                /// If salariedHourlyCodeIdNo == -10192 (Hourly) then PayGroup wold be Hourly else PayGrup wod be exempt
                int payGroup = salariedHourlyCodeIdNo == -10191 ? 2238 : 2237;
                DateTime from = DateTime.Today;
                try
                {
                    from = DateTime.Parse(personalInfoModuleFields[3].ToString().Trim());
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


                System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("SeqNo", typeof(Int32));
                seqNo.Value = 0;

                kronosContext.sp_saveEmpPaySetupCtrl(personId,
                                                     null,
                                                     seqNo,
                                                     from,
                                                     null,
                                                     2236, //Pay Agency - COPERVISION cARIBBEAN cORP
                                                     payGroup, //Pay Group 
                                                     -11039, //ResidenceLocation - Puerto Rico
                                                     false, //Benefit Pay Group Indicator
                                                     -10500, //Payroll Status - Active
                                                     -10107, // Employee Clasification - Regular
                                                     0,
                                                     null,
                                                     -10001,// Excemption Type
                                                     null,
                                                     null,
                                                     null);
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }
        }

        private static void ProcessPayStatusModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum, int positionIdNo)
        {
            DateTime from;
            string actionIndicator = string.Empty;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }

            try
            {
                from = DateTime.Parse(fields[3].ToString().Trim());
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 2, expected EffectiveFromDate(int), received: " + fields[3].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }
            try
            {
                tPAY_STATUS personPayStatus = kronosContext.tPAY_STATUS.Where(p => p.PersonIdNo == personIdNo && p.PayStatusFromEffectDate <= from && p.PayStatusToEffectDate >= from).FirstOrDefault();
                if (personPayStatus == null)
                {
                    personPayStatus = new tPAY_STATUS();
                    personPayStatus.PersonIdNo = personIdNo;
                }

                ///Fill Person Pay Status with values from extracted file
                personPayStatus = (tPAY_STATUS)FillObjectValues(fields, module, personPayStatus, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos     
                ///

                if (personPayStatus != null)
                {
                    int? basePay = personPayStatus.BasePayInd ? 1 : 0;

                    double? unitsPerWeek = null;
                    if (personPayStatus.StandardUnitsPerWeek.HasValue)
                    {
                        unitsPerWeek = (double)personPayStatus.StandardUnitsPerWeek.Value;
                    }

                    double? userDefined2 = null;
                    if (personPayStatus.PayStatusUserDefined2.HasValue)
                    {
                        userDefined2 = (double)personPayStatus.PayStatusUserDefined2.Value;
                    }

                    System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PayStatusSeqNo", typeof(Int32));
                    seqNo.Value = personPayStatus.PayStatusSeqNo;

                    kronosContext.sp_savePayStatusCtrl(personPayStatus.PersonIdNo,
                                                       personPayStatus.PayStatusSeqNo,
                                                       personPayStatus.Timestamp,
                                                       actionIndicator == "N" ? from : personPayStatus.PayStatusFromEffectDate,
                                                       personPayStatus.PayStatusToEffectDate,
                                                       positionIdNo == -1 ? personPayStatus.PositionIdNo : positionIdNo,///Position ==-1 means its not a new Hire so we use the same positionId else its a new hire and we received the positionIdNo as parameter
                                                       personPayStatus.PayReasonIdNo,
                                                       personPayStatus.PayStatusCompensationTypeIdNo,
                                                       personPayStatus.PayRateFrequencyCodeIdNo,
                                                       (double)personPayStatus.PayRate,
                                                       unitsPerWeek,
                                                       personPayStatus.AnnualizingFactor,
                                                       null,
                                                       personPayStatus.LastAnnualizedPayRate,
                                                       basePay,
                                                       personPayStatus.LastSalaryChangeDate,
                                                       personPayStatus.NextPayReviewDate,
                                                       personPayStatus.ShiftDiffIdNo,
                                                       personPayStatus.CurrencyCodeIdNo,
                                                       personPayStatus.PayStatusUserDefined1,
                                                       userDefined2,
                                                       null,
                                                       null,
                                                       null,
                                                       null,
                                                       null,
                                                       null);

                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK " + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s) " + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
            }
        }

        private static int ProcessPositionModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum, bool newHire, out int salariedHourlyCodeIdNo)
        {
            DateTime from;
            string actionIndicator = string.Empty;
            salariedHourlyCodeIdNo = 0;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return 0;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return 0;
            }

            try
            {
                from = DateTime.Parse(fields[3].ToString().Trim());
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 2, expected EffectiveFromDate(int), received: " + fields[3].ToString().Trim() + "#";
                logger.Error(ex);

                return 0;
            }
            try
            {
                tPERSON_POSITIONS personPosition = kronosContext.tPERSON_POSITIONS.Where(p => p.PersonIdNo == personIdNo && p.PositionFromEffectDate <= from && p.PositionToEffectDate >= from).FirstOrDefault();
                if (personPosition == null)
                {
                    personPosition = new tPERSON_POSITIONS();
                    personPosition.PersonIdNo = personIdNo;
                }

                ///Fill Person Position with values from extracted file
                personPosition = (tPERSON_POSITIONS)FillObjectValues(fields, module, personPosition, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos          

                if (personPosition != null)
                {
                    int? posPrimaryInd = personPosition.PositionPrimaryInd ? 1 : 0;
                    int? promotionInd = personPosition.PromotionInd ? 1 : 0;
                    int? exempStatusInd = personPosition.ExemptStatusInd ? 1 : 0;
                    int? seasonalWorkerInd = personPosition.SeasonalWorkerInd ? 1 : 0;

                    double? defined2 = null;
                    if (personPosition.PositionUserDefined2.HasValue)
                    {
                        defined2 = (double)personPosition.PositionUserDefined2;
                    }

                    int? primary = personPosition.PositionPrimaryInd ? 1 : 0;
                    int? promotion = personPosition.PromotionInd ? 1 : 0;
                    int? exemp = personPosition.ExemptStatusInd ? 1 : 0;
                    int? seasonal = personPosition.SeasonalWorkerInd ? 1 : 0;

                    double? userDefined2 = null;
                    if (personPosition.PositionUserDefined2.HasValue)
                    {
                        userDefined2 = (double)personPosition.PositionUserDefined2.Value;
                    }

                    kronosContext.sp_savePositionCtrl(personPosition.PersonIdNo,
                                                       personPosition.PositionIdNo,
                                                       personPosition.PositionSeqNo,
                                                       personPosition.Timestamp,
                                                       personPosition.PositionReasonIdNo,
                                                       actionIndicator == "N" ? from : personPosition.PositionFromEffectDate,
                                                       new DateTime(3000, 01, 01),
                                                       personPosition.SupervisorIdNo,
                                                       personPosition.PositionStartDate,
                                                       personPosition.Title,
                                                       personPosition.MailStop,
                                                       newHire ? -1 : primary,
                                                       promotion,
                                                       exemp,
                                                       personPosition.SalariedHourlyCodeIdNo,
                                                       seasonal,
                                                       personPosition.UnionCodeIdNo,
                                                       personPosition.UnionDate,
                                                       personPosition.PositionUserDefined1,
                                                       userDefined2,
                                                       personPosition.FTEHoursPerWeek,
                                                       personPosition.WorkersCompensationIdNo,
                                                       personPosition.CompLocationFactor,
                                                       null,
                                                       null,
                                                       null,
                                                       null);

                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK " + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s) " + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;
                    salariedHourlyCodeIdNo = personPosition.SalariedHourlyCodeIdNo;

                    return personPosition.PositionIdNo;
                }

                return 0;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
                return 0;
            }
        }

        private static void ProcessEmploymentStatusModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum, int positionIdNo)
        {
            DateTime from;
            string actionIndicator = string.Empty;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }

            try
            {
                from = DateTime.Parse(fields[3].ToString().Trim());
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 2, expected EffectiveFromDate(int), received: " + fields[3].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }
            try
            {
                tEMPLOYMENT_STATUS personEmpStatus = kronosContext.tEMPLOYMENT_STATUS.Where(p => p.PersonIdNo == personIdNo && p.EmploymentStatusFromEffectDate <= from && p.EmploymentStatusToEffectDate >= from).FirstOrDefault();
                if (personEmpStatus == null)
                {
                    personEmpStatus = new tEMPLOYMENT_STATUS();
                    personEmpStatus.PersonIdNo = personIdNo;
                }

                ///Fill Employment Status with values from extracted file
                personEmpStatus = (tEMPLOYMENT_STATUS)FillObjectValues(fields, module, personEmpStatus, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos
                if (personEmpStatus != null)
                {
                    int? fullTime = personEmpStatus.FullTimeInd ? 1 : 0;
                    bool rehire = personEmpStatus.Rehire != null && personEmpStatus.Rehire.ToUpper() == "Y" ? true : false;
                    double? defined4 = null;

                    if (personEmpStatus.EmploymentStatusUserDefined4 != null)
                    {
                        defined4 = (double)personEmpStatus.EmploymentStatusUserDefined4;
                    }

                    int searchedOrgCodeIdNo = 0;
                    if (positionIdNo != -1)
                    {
                        searchedOrgCodeIdNo = GetOrgCodeIdNo(positionIdNo, from);
                    }

                    kronosContext.sp_saveEmploymentStatusCtrl(personEmpStatus.PersonIdNo,
                                                                personEmpStatus.EmploymentStatusSeqNo,
                                                                personEmpStatus.Timestamp,
                                                                personEmpStatus.EmployeeStatusIdNo,
                                                                personEmpStatus.EmploymentStatus,
                                                                fullTime,
                                                                personEmpStatus.LastHireDate,
                                                                personEmpStatus.LeaveReturnDate,
                                                                personEmpStatus.I9FormOnFileIdNo,
                                                                personEmpStatus.I9RenewalDate,
                                                                positionIdNo != -1 ? searchedOrgCodeIdNo : personEmpStatus.OrgCodeIdNo,/// if positionId!= -1 means its a NewHire and we searched for the orgCodeIdNo for the position else we leave OrgCodeIdNo as it was 
                                                            personEmpStatus.OriginalHireDate,
                                                                personEmpStatus.RecruiterCode,
                                                                rehire,
                                                                personEmpStatus.SeniorityDate,
                                                                personEmpStatus.SourceOfHireCode,
                                                                personEmpStatus.TerminationTypeIdNo,
                                                                personEmpStatus.EmpNo,
                                                                personEmpStatus.EmploymentStatusReasonIdNo,
                                                                personEmpStatus.EmploymentStatusUserDefined1,
                                                                personEmpStatus.EmploymentStatusUserDefined2,
                                                                personEmpStatus.EmploymentStatusUserDefined3,
                                                                defined4,
                                                                actionIndicator == "N" ? from : personEmpStatus.EmploymentStatusFromEffectDate,
                                                                personEmpStatus.TerminationDate,
                                                                personEmpStatus.EmploymentStatusToEffectDate,
                                                                null,
                                                                null,
                                                                null,
                                                                null);



                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK" + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s)" + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
            }

        }

        private static int GetOrgCodeIdNo(int positionIdNo, DateTime fromEffectiveDate)
        {
            int orgCodeIdNo = 0;

            tSYSTEM_SETTINGS sett = kronosContext.tSYSTEM_SETTINGS.Where(s => s.ObjectType == "EmploymentStatusLevelNo").FirstOrDefault();
            if (sett != null)
            {
                int reportingTreeIdNo = 0;
                int organizationLevelNo = sett.ObjectNumber.HasValue ? sett.ObjectNumber.Value : 0;
                int.TryParse(sett.ObjectName, out reportingTreeIdNo);

                orgCodeIdNo = (from org in kronosContext.tORGANIZATIONS
                               join orgType in kronosContext.tORGANIZATION_TYPES on org.OrganizationTypeIdNo equals orgType.OrganizationTypeIdNo
                               join posOrg in kronosContext.tPOSITION_ORGS on org.OrgCodeIdNo equals posOrg.OrgCodeIdNo
                               where fromEffectiveDate >= org.OrganizationFromEffectDate
                                    && fromEffectiveDate <= org.OrganizationToEffectDate
                                    && fromEffectiveDate >= posOrg.PositionOrgFromEffectDate
                                    && fromEffectiveDate <= posOrg.PositionOrgToEffectDate
                                    && posOrg.PositionIdNo == positionIdNo
                                    && org.OrganizationLevelNo == organizationLevelNo
                                    && orgType.ReportingTreeIdNo == reportingTreeIdNo
                               select org.OrgCodeIdNo).FirstOrDefault();

            }

            return orgCodeIdNo;
        }

        private static void ProcessEmailModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum)
        {
            string actionIndicator = string.Empty;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }

            tEMAIL personEmail = kronosContext.tEMAILs.Where(e => e.PersonIdNo == personIdNo).OrderByDescending(e => e.EMailFromEffectDate).FirstOrDefault();
            if (personEmail == null)
            {
                personEmail = new tEMAIL();
                personEmail.PersonIdNo = personIdNo;
            }

            try
            {
                ///Fill email with values from extracted file
                personEmail = (tEMAIL)FillObjectValues(fields, module, personEmail, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos
                if (personEmail != null)
                {
                    int? primaryInd = personEmail.EMailPrimaryInd ? 1 : 0;

                    personEmail.EMailFromEffectDate = DateTime.Today;

                    kronosContext.sp_saveEmailCtrl(personEmail.PersonIdNo, actionIndicator == "N" ? 0 : personEmail.EMailSeqNo, primaryInd, personEmail.EMailAddress, actionIndicator == "N" ? DateTime.Today : personEmail.EMailFromEffectDate, personEmail.Timestamp);

                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK " + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s) " + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
            }
        }

        private static void ProcessPhoneModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum)
        {
            DateTime from;
            string actionIndicator = string.Empty;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }

            try
            {
                from = DateTime.Parse(fields[3].ToString().Trim());
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 2, expected EffectiveFromDate(int), received: " + fields[3].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }

            tPERSON_PHONES personPhone = kronosContext.tPERSON_PHONES.Where(p => p.PersonIdNo == personIdNo && p.PersonPhoneFromEffectDate <= from && p.PersonPhoneToEffectDate >= from).FirstOrDefault();
            if (personPhone == null)
            {
                personPhone = new tPERSON_PHONES();
                personPhone.PersonIdNo = personIdNo;
            }

            try
            {
                ///Fill phone with values from extracted file
                personPhone = (tPERSON_PHONES)FillObjectValues(fields, module, personPhone, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos
                if (personPhone != null)
                {
                    System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonPhoneSeqNo", typeof(Int32));
                    seqNo.Value = actionIndicator == "N" ? 0 : personPhone.PersonPhoneSeqNo;
                    personPhone.PersonPhoneSeqNo = actionIndicator == "N" ? 0 : personPhone.PersonPhoneSeqNo;

                    kronosContext.sp_savePhoneCtrl(personPhone.PersonIdNo,
                                                    seqNo,
                                                    personPhone.PersonPhoneTypeIdNo,
                                                    personPhone.PersonPhonePrimaryInd,
                                                    personPhone.PersonPhoneNo,
                                                    personPhone.PersonPhoneExt,
                                                    actionIndicator == "N" ? from : personPhone.PersonPhoneFromEffectDate,
                                                    personPhone.Timestamp);
                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK " + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s) " + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
            }
        }

        private static void ProcessAddressModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum)
        {
            DateTime from;
            string actionIndicator = string.Empty;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }

            try
            {
                from = DateTime.Parse(fields[3].ToString().Trim());
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 2, expected EffectiveFromDate(int), received: " + fields[3].ToString().Trim() + "#";
                logger.Error(ex);

                return;
            }
            try
            {

                tPERSON_ADDRESSES personAddr = kronosContext.tPERSON_ADDRESSES.Where(p => p.PersonIdNo == personIdNo && p.PersonAddressFromEffectDate <= from && p.PersonAddressToEffectDate >= from).FirstOrDefault();
                if (personAddr == null)
                {
                    personAddr = new tPERSON_ADDRESSES();
                    personAddr.PersonIdNo = personIdNo;
                }

                ///Fill address with values from extracted file
                personAddr = (tPERSON_ADDRESSES)FillObjectValues(fields, module, personAddr, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos
                if (personAddr != null)
                {
                    personAddr.PersonAddressSeqNo = actionIndicator == "N" ? 0 : personAddr.PersonAddressSeqNo;
                    System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonAddressSeqNo", typeof(Int32));
                    seqNo.Value = actionIndicator == "N" ? 0 : personAddr.PersonAddressSeqNo;

                    kronosContext.sp_saveAddressCtrl(personAddr.PersonIdNo,
                                                        seqNo,
                                                        personAddr.Timestamp,
                                                        actionIndicator == "N" ? from : personAddr.PersonAddressFromEffectDate,
                                                        personAddr.PersonAddressToEffectDate,
                                                        personAddr.PersonAddressTypeIdNo,
                                                        personAddr.PersonAddressPrimaryInd,
                                                        personAddr.PersonAddress1,
                                                        personAddr.PersonAddress2,
                                                        personAddr.PersonAddressCity,
                                                        personAddr.PersonAddressStateProvinceIdNo,
                                                        personAddr.PersonAddressCountryCodeIdNo,
                                                        personAddr.PersonAddressCountryCodeIdNo,
                                                        personAddr.PersonAddressPostalCode);

                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK " + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s) " + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
            }
        }

        private static int ProcessPersonalInfoModule(string[] fields, Module module, int personIdNo, ExtractedFileModule extractModule, string legacyEmpNum)
        {
            DateTime from;
            string actionIndicator = string.Empty;
            try
            {
                actionIndicator = fields[1].ToString().ToUpper().Trim();
                if (actionIndicator != "U" && actionIndicator != "N")
                {
                    extractModule.Log += "Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim() + "#";
                    logger.Error("Invalid action indicator, accepted values are U - for update N for new record, received: " + fields[1].ToString().Trim());
                    return -1;
                }
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 1, expected ActionIndicator(int), received: " + fields[1].ToString().Trim() + "#";
                logger.Error(ex);

                return -1;
            }

            try
            {
                from = DateTime.Parse(fields[3].ToString().Trim());
            }
            catch (Exception ex)
            {
                extractModule.Log += "Invalid module field at position 2, expected EffectiveFromDate(int), received: " + fields[3].ToString().Trim() + "#";
                logger.Error(ex);

                return -1;
            }

            try
            {

                tPERSON person = kronosContext.tPERSONS.Where(p => p.PersonIdNo == personIdNo && p.PersonFromEffectDate <= from && p.PersonToEffectDate >= from).FirstOrDefault();
                if (person == null)
                {
                    person = new tPERSON();
                }

                ///Fill Person with values from extracted file
                person = (tPERSON)FillObjectValues(fields, module, person, extractModule);

                ///If object is not null then all the fields were extracted without error then we can send the changes to Kronos
                if (person != null)
                {
                    System.Data.Entity.Core.Objects.ObjectParameter idNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonIdNo", typeof(Int32));
                    idNo.Value = person.PersonIdNo;

                    System.Data.Entity.Core.Objects.ObjectParameter seqNo = new System.Data.Entity.Core.Objects.ObjectParameter("PersonSeqNo", typeof(Int32));
                    seqNo.Value = person.PersonSeqNo;

                    double? personUserDefined4 = null;

                    if (person.PersonUserDefined4.HasValue)
                        personUserDefined4 = (double)person.PersonUserDefined4.Value;

                    kronosContext.sp_savePersonCtrl(
                        idNo,
                       seqNo,
                       person.BirthDate,
                       person.DisabledInd,
                       person.DisabilityTypeIdNo,
                       person.DisabledVetInd,
                       person.FirstName,
                       person.LastName,
                       person.NameSuffix,
                       person.MaritalStatusIdNo,
                       person.MiddleName,
                       person.MilitaryServiceIdNo,
                       person.MilitarySeparationDate,
                       person.Nickname,
                       person.EthnicCodeIdNo,
                       person.Salutation,
                       person.GenderIdNo,
                       person.SmokerInd,
                       person.PersonTaxIdNo,
                       person.VietnamVetInd,
                       person.OtherVetInd,
                       person.BloodTypeCodeIdNo,
                       person.BloodRHFactorIdNo,
                       person.Height,
                       person.Weight,
                       person.ForeignNationalRegistrationNo,
                       person.CitizenshipIdNo,
                       person.VisaExpirationDate,
                       person.VisaType,
                       person.PersonUserDefined1,
                       person.PersonUserDefined2,
                       person.PersonUserDefined3,
                       personUserDefined4,
                        actionIndicator == "N" ? from : person.PersonFromEffectDate,
                        person.PersonToEffectDate,
                        person.Timestamp,
                        null,
                        person.PersonUserDefined5,
                        person.PersonUserDefined6,
                        person.ArmedForcesMedalVetInd,
                        person.CampaignVetInd);

                    ///If no error or warning was reported then status = OK
                    if (string.IsNullOrEmpty(extractModule.Status))
                    {
                        extractModule.Status = "OK";
                        logger.Info("Module finished: OK " + legacyEmpNum);
                    }
                    else
                    {
                        logger.Warn("Module finished with warning(s) or error(s) " + legacyEmpNum);
                    }

                    extractModule.EmpId = legacyEmpNum;

                    return (int)idNo.Value;
                }

                return -1;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                extractModule.Status = "error";
                extractModule.Log += (ex.InnerException != null ? ex.InnerException.Message : ex.Message) + "#";
                extractModule.EmpId = legacyEmpNum;
                return -1;
            }
        }

        private static object FillObjectValues(string[] fields, Module module, object obj, ExtractedFileModule extractModule)
        {
            ///This method gets the values from the fields ands tries to cast it to the field type defined on the table ModuleField
            ///if cast is succeed the value is assigned to the corresponding mapping property on kronos object by reflection
            ///else warining or error is logged on ExtractedFileModule

            ///Iterate throug all fields of the module
            foreach (ModuleField field in module.ModuleFields.OrderBy(f => f.Position).ToList())
            {
                string fieldInfo = fields[field.Position].ToString().Trim();

                ///If field has matching with kronos and field changed (!="~") then procces the field else do nothing
                if (field.KronosName != null && fieldInfo != "~")
                {
                    switch (field.Type)
                    {
                        ///FieldType String
                        case "string":
                            {
                                ///Validate length and if exceed truncate it
                                if (fieldInfo.Length > field.Length)
                                {
                                    fieldInfo = fieldInfo.Substring(0, (int)field.Length);
                                    string log = "Field " + field.Name + " exceeded max permited length of " + field.Length + " value was truncated#";
                                    extractModule.Log += log;
                                    logger.Warn(log);

                                    if (string.IsNullOrEmpty(extractModule.Status))
                                    {
                                        extractModule.Status = "warning";
                                    }
                                }

                                obj.GetType().GetProperty(field.KronosName).SetValue(obj, fieldInfo);

                                break;
                            }

                        case "int":
                        case "foreignkey":
                            {
                                int intValue = 0;

                                try
                                {
                                    intValue = int.Parse(fieldInfo);
                                }
                                catch (Exception ex)
                                {
                                    string log = "Invalid field " + field.Name + " at position " + field.Position.ToString() + " ,expected value of type  " + field.Type + ", value received was " + fieldInfo + "#";
                                    extractModule.Log += log;
                                    logger.Error(log);
                                    extractModule.Status = "error";
                                    return null;
                                }

                                obj.GetType().GetProperty(field.KronosName).SetValue(obj, intValue);

                                break;
                            }

                        case "date":
                            {
                                DateTime dateValue;

                                try
                                {
                                    dateValue = DateTime.Parse(fieldInfo);
                                }
                                catch (Exception ex)
                                {
                                    string log = "Invalid field " + field.Name + " at position " + field.Position.ToString() + ", expected value of type  " + field.Type + ", value received was " + fieldInfo + "#";
                                    extractModule.Log += log;
                                    logger.Error(log);
                                    extractModule.Status = "error";
                                    return null;
                                }

                                obj.GetType().GetProperty(field.KronosName).SetValue(obj, dateValue);


                                break;
                            }

                        case "decimal":
                            {
                                Decimal decimalValue;

                                try
                                {
                                    decimalValue = decimal.Parse(fieldInfo);
                                }
                                catch (Exception ex)
                                {
                                    string log = "Invalid field " + field.Name + " at position " + field.Position.ToString() + ", expected value of type  " + field.Type + ", value received was " + fieldInfo + "#";
                                    extractModule.Log += log;
                                    logger.Error(log);
                                    extractModule.Status = "error";
                                    return null;
                                }

                                obj.GetType().GetProperty(field.KronosName).SetValue(obj, decimalValue);


                                break;
                            }

                        case "bool":
                            {
                                bool boolValue;

                                try
                                {
                                    ///If value sended was 1 or 0 then set field info to "true" or "false"
                                    if (fieldInfo == "1")
                                    {
                                        fieldInfo = "true";
                                    }

                                    if (fieldInfo == "0")
                                    {
                                        fieldInfo = "false";
                                    }

                                    boolValue = bool.Parse(fieldInfo);
                                }
                                catch (Exception ex)
                                {
                                    string log = "Invalid field " + field.Name + " at position " + field.Position.ToString() + " expected value of type  " + field.Type + ", value received was " + fieldInfo + "#";
                                    extractModule.Log += log;
                                    logger.Error(log);
                                    extractModule.Status = "error";
                                    return null;
                                }

                                obj.GetType().GetProperty(field.KronosName).SetValue(obj, boolValue);

                                break;
                            }
                        case "positioncode":
                            {
                                tPOSITION_CODES position = kronosContext.tPOSITION_CODES.Where(p => p.PositionCode == fieldInfo).FirstOrDefault();
                                if (position == null)
                                {
                                    string log = "Invalid field " + field.Name + " at position " + field.Position.ToString() + " no position found for position code " + fieldInfo + "#";
                                    extractModule.Log += log;
                                    logger.Error(log);
                                    extractModule.Status = "error";
                                    return null;
                                }

                                obj.GetType().GetProperty(field.KronosName).SetValue(obj, position.PositionIdNo);

                                break;
                            }
                        case "personid":
                            {
                                tEMPLOYMENT_STATUS empStatus = kronosContext.tEMPLOYMENT_STATUS.Where(p => p.EmpNo == fieldInfo).FirstOrDefault();
                                if (empStatus == null)
                                {
                                    string log = "Invalid field " + field.Name + " at position " + field.Position.ToString() + " no Employee found for Emp Id  " + fieldInfo + "#";
                                    extractModule.Log += log;
                                    logger.Error(log);
                                    extractModule.Status = "error";
                                    return null;
                                }

                                obj.GetType().GetProperty(field.KronosName).SetValue(obj, empStatus.PersonIdNo);

                                break;
                            }
                    }
                }
            }

            return obj;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            logger.Error(e.ExceptionObject);
        }
    }
}
