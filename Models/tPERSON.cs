//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tPERSON
    {
        public int PersonIdNo { get; set; }
        public int PersonSeqNo { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public bool DisabledInd { get; set; }
        public int DisabilityTypeIdNo { get; set; }
        public bool DisabledVetInd { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NameSuffix { get; set; }
        public string FormerLastName { get; set; }
        public int MaritalStatusIdNo { get; set; }
        public string MiddleName { get; set; }
        public int MilitaryServiceIdNo { get; set; }
        public Nullable<System.DateTime> MilitarySeparationDate { get; set; }
        public string Nickname { get; set; }
        public int EthnicCodeIdNo { get; set; }
        public string Salutation { get; set; }
        public int GenderIdNo { get; set; }
        public bool SmokerInd { get; set; }
        public string PersonTaxIdNo { get; set; }
        public bool VietnamVetInd { get; set; }
        public bool OtherVetInd { get; set; }
        public bool ArmedForcesMedalVetInd { get; set; }
        public bool CampaignVetInd { get; set; }
        public int BloodTypeCodeIdNo { get; set; }
        public int BloodRHFactorIdNo { get; set; }
        public string Height { get; set; }
        public Nullable<short> Weight { get; set; }
        public string ForeignNationalRegistrationNo { get; set; }
        public int CitizenshipIdNo { get; set; }
        public bool VisaInd { get; set; }
        public Nullable<System.DateTime> VisaStartDate { get; set; }
        public Nullable<System.DateTime> VisaExpirationDate { get; set; }
        public string VisaType { get; set; }
        public bool WorkAllowanceInd { get; set; }
        public Nullable<System.DateTime> WorkAllowanceStartDate { get; set; }
        public Nullable<System.DateTime> WorkAllowanceExpirationDate { get; set; }
        public string SecondaryTitle { get; set; }
        public string PersonUserDefined1 { get; set; }
        public string PersonUserDefined2 { get; set; }
        public string PersonUserDefined3 { get; set; }
        public Nullable<decimal> PersonUserDefined4 { get; set; }
        public string PersonUserDefined5 { get; set; }
        public string PersonUserDefined6 { get; set; }
        public System.DateTime PersonFromEffectDate { get; set; }
        public System.DateTime PersonToEffectDate { get; set; }
        public System.DateTime PersonChangeDate { get; set; }
        public byte[] Timestamp { get; set; }
        public string CountryCode { get; set; }
    }
}
